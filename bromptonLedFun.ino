#include <FastLED.h>

#define NUM_LEDS 50
#define DATA_PIN 2

CRGB leds[NUM_LEDS];
CHSV spectrumcolor;

void setup() { 
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

uint8_t val = 100;

void rainbowBouncer(int count) {
  for (int j=0; j<count; j++) {
    for (int i=0; i<25; i++) {
      uint8_t h = (255 / ((NUM_LEDS-2)/2)) * i;
      leds[i] = CHSV(h, 255, val);
      leds[NUM_LEDS-1-(i)] = CHSV(h, 255, val);
      FastLED.show();
      delay(20);
      leds[i] = 0x000000;
      leds[(NUM_LEDS-1-i)] = 0x000000;
      FastLED.show();
    }
    for (int i=25; i>0; i--) {
      uint8_t h = (255 / ((NUM_LEDS-2)/2)) * i;
      leds[i] = CHSV(h, 255, val);
      leds[NUM_LEDS-1-(i)] = CHSV(h, 255, val);
      FastLED.show();
      delay(20);
      leds[i] = 0x000000;
      leds[(NUM_LEDS-1-i)] = 0x000000;
      FastLED.show();
    }
  }
}

void police(int count) {
  for (int j=0; j<count; j++) {
    for (int i=0; i<NUM_LEDS; i++) {
      if (i<8 || i>16 && i<25 || i>32 && i<42) {
       leds[i] = CHSV(0,255,val-20); 
      } else {
        leds[i] = CHSV(170,255,val);
      }
    }
    FastLED.show();
    delay(150);
    for (int i=0; i<NUM_LEDS; i++) {
      if (i<8 || i>16 && i<25 || i>32 && i<42) {
       leds[i] = CHSV(170,255,val); 
      } else {
        leds[i] = CHSV(0,255,val-20);
      }
    }
    FastLED.show();
    delay(150);
  }
}

void rainbowFader(int count) {
 for (int h=0; h<count; h++) {
  for (int i=0; i<255; i++) {
    for (int j=0; j<25; j++) {
     leds[j] = CHSV(i, 255, val);
    }
    FastLED.show();
    delay(20);
  }
 } 
}

void loop() {
  rainbowFader(10);
  police(20);
  rainbowBouncer(15);
}
